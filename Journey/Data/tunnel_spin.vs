#version 440
precision highp float;

uniform float uTime;
uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;
uniform mat3 uNormalMatrix;

varying vec3 vPosition;
varying vec3 vNormal;

attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec2 aTextureCoordinate;

varying vec2 vTextureCoordinate;

void main()
{
    gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition, 1.0);
    vec4 pos4 = uModelViewMatrix * vec4(aVertexPosition, 1.0);
    vPosition = vec3(pos4) / pos4.w;
    vNormal = uNormalMatrix * aVertexNormal;
    vTextureCoordinate = aTextureCoordinate;
}
