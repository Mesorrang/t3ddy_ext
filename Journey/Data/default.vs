#version 440
precision highp float;

attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;
uniform mat3 uNormalMatrix;

varying vec3 vNormal;
varying vec3 vPosition;

uniform float uTime;

void main() {
    vec3 avert = aVertexPosition;

    // Create a looping for the object
    float fpart = fract(uTime / 64.f) * 100.f;

    // Define and assign rotation for hte object
    mat3 full_rot;
    {
        mat3 rotation_x = mat3(vec3(1, 0, 0), vec3(0, cos(uTime), -sin(uTime)),
                vec3(0, sin(uTime), cos(uTime)));

        mat3 rotation_y = mat3(vec3(cos(uTime), 0, sin(uTime)), vec3(0, 1, 0),
                vec3(-sin(uTime), 0, cos(uTime)));

        mat3 rotation_z = mat3(vec3(cos(uTime), -sin(uTime), 0),
                vec3(sin(uTime), cos(uTime), 0), vec3(0, 0, 1));

        full_rot = rotation_x * rotation_z;
    }

    vec4 position = uProjectionMatrix * uModelViewMatrix * vec4(avert, 1.0);

    vec4 pos4 = uModelViewMatrix * vec4(avert, 1.0);

    vPosition = vec3(pos4) / pos4.w;
    vNormal = uNormalMatrix * aVertexNormal;

    // Assign object rotation and depth
    vec3 normal = full_rot * position.xyz;
    gl_Position = vec4(normal, 1.f * fpart);

}
