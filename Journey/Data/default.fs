#version 440

precision mediump float;
#define PI 3.1415926535897932384626433832795

uniform float uTime;
varying vec3 vNormal;
varying vec3 vPosition;

void main() {
    float v = 0.0;
    float b = 0.0;

    float intensity = 4.0;

    vec3 c = vNormal * intensity - intensity / 2.0;

    // Simple vertical left-right waves
    v += sin((c.z + uTime));

    // Simple horizontal top-bottom waves
    v += sin((c.z + uTime) / 2.0);

    // Simple topright-bottomleft waves
    v += sin((c.x + c.y + uTime) / 2.0);

    // Moving circular waves
    c += intensity / 2.0 * vec3(sin(uTime / 3.0), cos(uTime / 2.0), 1.0);
    v += sin(sqrt(c.x * c.x + c.y * c.y + c.z * c.z + 1.0) + uTime);
    v = v / 2.0;

    // Assign color to the plasma and add fade at a certain position
    vec3 col = vec3(sin(v * PI), sin(v * PI), sin(v * PI));

    gl_FragColor = vec4(col, 0.0);

}
