// Plasma effects:
//      http://www.bidouille.org/prog/plasma
// ShaderToy examples:
//      https://www.shadertoy.com/view/XssGDr
//      https://www.shadertoy.com/view/ldlXz7

#version 440
precision highp float;

#define PI 3.1415926535897932384626433832795

uniform float uTime;
uniform float uScreenWidth;
uniform float uScreenHeight;
varying vec3 vNormal;

vec3 tunnel(in vec2 pix, in int useAngle);

void main() {
    vec3 color = vec3(1.0, 1.0, 1.0);

    vec2 tc = gl_FragCoord.xy / vec2(uScreenWidth, uScreenHeight);

    float tunnel_depth;

    // To have more than one plasma tunnel effect active, we need to calculate the plasma from the start.
    vec3 tunnel_lines = tunnel(tc, 1);
    vec3 tunnel_plasma = tunnel(tc, 0);
    tunnel_depth = tunnel_plasma.z;

    // Get effect coordinates and depth
    float scale = 20.0;
    tunnel_lines = tunnel_lines * scale - scale / 2.0;
    tunnel_plasma = (tunnel_plasma * scale - scale / 2.0) / 5.0;

    vec2 plasma;
    float vx = 0.0;
    float tunnel_wall_plasma;

    float slow_tunnel = 0.0;
    float fast_tunnel = 0.0;
    float fast_tunnel_speed_factor = 10.0;

    // The depth of the current pixel is used as a parameter for the plasma.
    float line_param = tunnel_depth * 5;
    float plasma_param = tunnel_depth;

    // Create fast tunnel lines (main lines)
    {
        fast_tunnel += cos(tunnel_lines.x + line_param);
        fast_tunnel += cos(tunnel_lines.y + line_param) / 2;
        fast_tunnel += cos(tunnel_lines.x + tunnel_lines.y + line_param) / 2;
    }

    // Create slow tunnel lines (effect lines)
    {
        slow_tunnel += sin(tunnel_lines.x + line_param);
        slow_tunnel += sin(tunnel_lines.y + line_param) / 2;
        slow_tunnel += sin(tunnel_lines.x + tunnel_lines.y + line_param) / 2;
    }

    // Create tunnel plasma
    {
        // Simple vertical left-right waves
        tunnel_wall_plasma += sin(tunnel_plasma.x + plasma_param);
        tunnel_wall_plasma += sin((tunnel_plasma.y + plasma_param) / 2.0);

        // Simple topright-bottomleft waves
        tunnel_wall_plasma += sin((tunnel_plasma.x + tunnel_plasma.y + plasma_param) / 2.0);
        plasma += scale / 2.0 * vec2(sin(plasma_param / 3.0), cos(plasma_param / 2.0));
        tunnel_wall_plasma += sin(sqrt(plasma.x * plasma.x + plasma.y * plasma.y + 1.0) + plasma_param);
        tunnel_wall_plasma = tunnel_wall_plasma / 2.0;
    }

    // Add fast moving blue tunnel lines
    vec3 col;
    col = vec3(0, sin(fast_tunnel) / 2.0, sin(fast_tunnel));
    col += vec3(0, cos(fast_tunnel) / 2.0, cos(fast_tunnel));

    // Add slow moving orange tunnel lines
    col += vec3(sin(slow_tunnel), sin(slow_tunnel) / 2, 0);
    col += vec3(cos(slow_tunnel), cos(slow_tunnel) / 2, 0);

    // Add orange plasma effect
    col += vec3(sin(tunnel_wall_plasma * PI + 2 * PI / 3), sin(tunnel_wall_plasma * PI + 2 * PI / 3) / 2, 0);
    col += vec3(cos(tunnel_wall_plasma * PI), cos(tunnel_wall_plasma * PI) / 2, 0);

    // Hide the end of the tunnel
    col = col * clamp(2.0 / tunnel_depth, 0.0, 1.0);

    gl_FragColor = vec4(col, 1.);

}

vec3 tunnel(in vec2 pix, in int tunnelType)
{
    float z;
    float aspect = uScreenWidth / uScreenHeight;

    // Moves the view around to look at the sides of the tunnel
    vec2 center;
    {
        if (tunnelType != 0)
            center = vec2(cos(uTime * 0.8), cos(uTime * 0.8));
        else
            center = vec2(cos(uTime * 0.8), cos(uTime * 0.8));
    }

    // Centers view on viewport
    vec2 pt = (pix * 2.0 - 1.0) * vec2(aspect, 1.0);

    // Define tunnel central position
    vec2 dir = pt - center;

    // Calculate the distance of this pixel from the center of the view.
    float dist = sqrt(dot(dir, dir));
    {
        z = 2.0 / dist;
    }

    // Return plasma or line tunnels with different intensities
    if (tunnelType != 0)
        return vec3(uTime * 0.3, z + uTime * 0.5, z);
    else
        return vec3(dir.x + uTime * 0.8, dir.y + uTime * 5.0, z);
}