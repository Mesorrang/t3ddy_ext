#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "T3ddy.h"

using namespace T3ddy;

#define LEN_OF(arr) (sizeof(arr)/sizeof(arr[0]))

class Demo: public Game {
public:
	Demo() {
	}

	~Demo() {
		delete bear;
		delete tunnel;
		delete background;
		delete material;
	}

	bool init(int width, int height, bool fullscreen, bool enableSwapControl) {

		if (!Game::init("Demo", width, height, fullscreen, enableSwapControl))
			return false;

		bear = Mesh::fromFile("Data/bear3.obj");

		if (!bear)
			return false;

		bear->setPosition(Vec3(0, 0, -1.25));
		bear->setScale(1.3);

		material = Shader::fromFile("Data/default.vs", "Data/default.fs");

		if (!material || !material->isValid())
			return false;

		background = Shader::fromFile("Data/tunnel_spin.vs",
				"Data/tunnel_spin.fs");

		camera.setPosition(Vec3(0.0f, 0.0f, 1.0f));

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0f);

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK);

		return true;
	}

	void render() {

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (true) {

			const GLfloat vertices[] = { -1.0f, -1.0f, +1.0f, -1.0f, +1.0f,
					+1.0f, -1.0f, +1.0f, };

			const GLuint indices[] = { 0, 1, 2, 0, 2, 3, };
			Vec3 lightPosition;

			background->bind();

			Mat4 P = Mat4::makeIdentity();
			Mat4 MV = Mat4::makeIdentity();

			int width, height;
			SDL_GetWindowSize(screen, &width, &height);

			GLfloat w = (GLfloat) width;
			GLfloat h = (GLfloat) height;

			background->setUniform("uProjectionMatrix", P);
			background->setUniform("uModelViewMatrix", MV);
			background->setUniform("uTime", Game::timestamp());
			background->setUniform("uScreenWidth", w);
			background->setUniform("uScreenHeight", h);

			background->setVertexAttribPointer("aVertexPosition", 2, GL_FLOAT,
					GL_FALSE, 0, vertices);

			glDrawElements(GL_TRIANGLES, LEN_OF(indices), GL_UNSIGNED_INT,
					indices);

		}

		glClear(GL_DEPTH_BUFFER_BIT);

		if (true) {

			int width, height;
			SDL_GetWindowSize(screen, &width, &height);

			GLfloat aspect = (GLfloat) width / (GLfloat) height;

			Mesh::RenderParams params;

			params.projectionMatrix = Mat4::makePerspective(45.0f, aspect, 1.0f,
					10000.0f);
			params.viewMatrix = camera.getViewMatrix();

			params.shader = material;
			params.ambientColor = Vec3(0.0f, 0.25f, 0.0f);
			params.diffuseColor = Vec3(0.5f, 0.0f, 0.0f);
			params.specularColor = Vec3(1.0f, 1.0f, 1.0f);
			params.time = Game::timestamp();

			bear->render(params);

		}

		glClear(GL_DEPTH_BUFFER_BIT);

		SDL_GL_SwapWindow(screen);

	}

	void handleKeyDown(SDL_Event *event) {
		SDL_Keycode key = event->key.keysym.sym;

		switch (key) {
		case SDLK_w:
			velocity.z = -1.0f;
			break;
		case SDLK_a:
			velocity.x = -1.0f;
			break;
		case SDLK_s:
			velocity.z = +1.0f;
			break;
		case SDLK_d:
			velocity.x = +1.0f;
			break;
		case SDLK_PAGEUP:
			velocity.y = +1.0f;
			break;
		case SDLK_PAGEDOWN:
			velocity.y = -1.0f;
			break;
		default:
			Game::handleKeyDown(event);
			break;
		}
	}

	void handleKeyUp(SDL_Event *event) {
		SDL_Keycode key = event->key.keysym.sym;

		switch (key) {
		case SDLK_w:
			velocity.z = 0.0f;
			break;
		case SDLK_a:
			velocity.x = 0.0f;
			break;
		case SDLK_s:
			velocity.z = 0.0f;
			break;
		case SDLK_d:
			velocity.x = 0.0f;
			break;
		case SDLK_PAGEUP:
			velocity.y = 0.0f;
			break;
		case SDLK_PAGEDOWN:
			velocity.y = 0.0f;
			break;
		case SDLK_SPACE:
			paused = !paused;
			break;
		default:
			Game::handleKeyUp(event);
			break;
		}
	}

	void handleMouseDown(SDL_Event *event) {
		switch (event->button.button) {
		case SDL_BUTTON_LEFT:
			velocity.z = +1.0f;
			break;
		case SDL_BUTTON_MIDDLE:
			break;
		case SDL_BUTTON_RIGHT:
			velocity.z = -1.0f;
			break;
		default:
			break;
		}
	}

	void handleMouseUp(SDL_Event *event) {
		velocity.z = 0.0f;
	}

private:
	Mesh *bear;
	Mesh *tunnel;
	Shader *material;
	Shader *background;
	Camera camera;
	Vec3 velocity;
	bool paused;
};

void usage(void) {
	printf("Options:\n");
	printf("-w WxH: set window size\n");
	printf("-f: enable fullscreen\n");
	printf("-v: enable GL swap control\n");
	exit(0);
}

int main(int argc, char **argv) {
	int width = 1280;
	int height = 720;
	bool fullscreen = false;
	bool enableSwapControl = false;
	int c;

	while ((c = getopt(argc, argv, "w:fv")) != EOF) {
		switch (c) {
		case 'w':
			sscanf(optarg, "%dx%d", &width, &height);
			break;
		case 'f':
			fullscreen = true;
			break;
		case 'v':
			enableSwapControl = true;
			break;
		default:
			usage();
			break;
		}
	}

	Demo *t = new Demo();
	if (t->init(width, height, fullscreen, enableSwapControl))
		t->run();
	delete t;
	return 0;
}
