#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "T3ddy.h"

using namespace T3ddy;

class Hemi: public Game
{
public:
	Hemi()
	{
	}

	~Hemi()
	{
		for (auto o: objects) delete o;
		delete shader;
	}

	bool init(int width, int height, bool fullscreen, bool enableSwapControl)
	{
		if (!Game::init("Hemispherical lighting", width, height, fullscreen, enableSwapControl))
			return false;

		shader = Shader::fromFile("Data/hemi.vs", "Data/hemi.fs");

		if (!shader)
			return false;

		if (!shader->isValid())
			return false;

		for (int x = 0; x < 3; x++)
		{
			for (int y = 0; y < 3; y++)
			{
				for (int z = 0; z < 3; z++)
				{
					Mesh *mesh = Mesh::fromFile("Data/bear3.obj");
					mesh->setPosition(Vec3(x,y,z)-Vec3(1.0f,1.0f,1.0f));
					mesh->setRotation(Vec3(x,y,z)-Vec3(1.0f,1.0f,1.0f));
					objects.push_back(mesh);
				}
			}
		}

		camera.setPosition(Vec3(0.0f, 0.0f, 0.0f));

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0f);

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK);

		return true;
	}

	void update(float dt)
	{
		camera.updateOrientation(dt*deltaHorizontalAngle, dt*deltaVerticalAngle);
		camera.updatePosition(dt*cameraMovement);

		GLfloat t = Game::timestamp();
		GLfloat a = 1.0;
		GLfloat b = 2.0;
		GLfloat c = 5.0;

		lightPosition = Vec3(c*sinf(a*t/2.0)*cosf(b*t/2.0),0.0f,c*sinf(a*t/2.0)*sinf(b*t/2.0));

        angle += 20.0f * dt;
        if (angle > 360.0f)
                angle -= 360.0f;

		for (auto o: objects)
		{
			o->setAngle(angle);
			o->update(dt);
		}
	}

	void render()
	{
		Vec3 ambientColor = Vec3::fromRGB(0x000033);

		glClearColor(ambientColor.x, ambientColor.y, ambientColor.z, 1.0f);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		int width, height;
		SDL_GetWindowSize(screen, &width, &height);


		Mesh::RenderParams params;

		params.shader = this->shader;
		GLfloat aspect = (GLfloat)width / (GLfloat)height;
		params.projectionMatrix = Mat4::makePerspective(45.0f, aspect, 1.0f, 100.0f);
		params.viewMatrix = camera.getViewMatrix();

		params.lightPosition = camera.getViewMatrix() * lightPosition;
		params.skyColor = Vec3::fromRGB(0xAACCFF);
		params.groundColor = Vec3::fromRGB(0x000033);

		for (auto o: objects) o->render(params);

		SDL_GL_SwapWindow(screen);
	}

	void handleKeyDown(SDL_Event *event)
	{
		SDL_Keycode key = event->key.keysym.sym;

		switch (key)
		{
		case SDLK_w:
			cameraMovement.z = +1.0f;
			break;
		case SDLK_a:
			cameraMovement.x = -1.0f;
			break;
		case SDLK_s:
			cameraMovement.z = -1.0f;
			break;
		case SDLK_d:
			cameraMovement.x = +1.0f;
			break;
		case SDLK_PAGEUP:
			cameraMovement.y = +1.0f;
			break;
		case SDLK_PAGEDOWN:
			cameraMovement.y = -1.0f;
			break;
		default:
			Game::handleKeyDown(event);
			break;
		}
	}

	void handleKeyUp(SDL_Event *event)
	{
		SDL_Keycode key = event->key.keysym.sym;

		switch (key)
		{
		case SDLK_w:
			cameraMovement.z = 0.0f;
			break;
		case SDLK_a:
			cameraMovement.x = 0.0f;
			break;
		case SDLK_s:
			cameraMovement.z = 0.0f;
			break;
		case SDLK_d:
			cameraMovement.x = 0.0f;
			break;
		case SDLK_PAGEUP:
			cameraMovement.y = 0.0f;
			break;
		case SDLK_PAGEDOWN:
			cameraMovement.y = 0.0f;
			break;
		case SDLK_SPACE:
			int width, height;
			SDL_GetWindowSize(screen, &width, &height);
			SDL_WarpMouseInWindow(screen, width/2, height/2);
			break;
		default:
			Game::handleKeyUp(event);
			break;
		}
	}

	void handleMouseMotion(SDL_Event *event)
	{
		GLfloat x = event->motion.x;
		GLfloat y = event->motion.y;

		int width, height;
		SDL_GetWindowSize(screen, &width, &height);

		GLfloat w = (GLfloat)width;
		GLfloat h = (GLfloat)height;

		GLfloat sensitivity = 50.0f;

		deltaHorizontalAngle = sensitivity * Tools::smoothstep(x, 0.0f, -1.0f, w, +1.0f);
		deltaVerticalAngle = sensitivity * Tools::smoothstep(y, 0.0f, -1.0f, h, +1.0f);
	}

	void handleMouseDown(SDL_Event *event)
	{
		switch (event->button.button)
		{
		case SDL_BUTTON_LEFT:
			cameraMovement.z = +1.0f;
			break;
		case SDL_BUTTON_MIDDLE:
			camera.setPosition(Vec3(0.0f, 0.0f, 3.0f));
			camera.setDirection(Vec3(0.0f, 0.0f, -1.0f));

			int width, height;
			SDL_GetWindowSize(screen, &width, &height);
			SDL_WarpMouseInWindow(screen, width/2, height/2);
			break;
		case SDL_BUTTON_RIGHT:
			cameraMovement.z = -1.0f;
			break;
		default:
			break;
		}
	}

	void handleMouseUp(SDL_Event *event)
	{
		cameraMovement.z = 0.0f;
	}

private:
	Shader *shader;
	std::vector<Mesh*> objects;
	Camera camera;
	GLfloat deltaHorizontalAngle;
	GLfloat deltaVerticalAngle;
	Vec3 cameraMovement;
	Vec3 lightPosition;
	GLfloat angle;
};

void usage(void)
{
	printf("Options:\n");
	printf("-w WxH: set window size\n");
	printf("-f: enable fullscreen\n");
	printf("-v: enable GL swap control\n");
	exit(0);
}

int main(int argc, char **argv)
{
	int width = 1280;
	int height = 720;
	bool fullscreen = false;
	bool enableSwapControl = false;
	int c;

	while ((c = getopt(argc, argv, "w:fv")) != EOF)
	{
		switch (c)
		{
		case 'w':
			sscanf(optarg, "%dx%d", &width, &height);
			break;
		case 'f':
			fullscreen = true;
			break;
		case 'v':
			enableSwapControl = true;
			break;
		default:
			usage();
			break;
		}
	}

	Hemi *hemi = new Hemi();
	if (hemi->init(width, height, fullscreen, enableSwapControl))
		hemi->run();
	delete hemi;
	return 0;
}
