#pragma once

#include "Common.h"

namespace T3ddy
{

class Game
{
public:
	Game();
	virtual ~Game();
	virtual bool init(const char *title, int width, int height, bool fullscreen = false, bool enableSwapControl = false);
	void run();
	virtual void handleKeyDown(SDL_Event *event);
	virtual void handleKeyUp(SDL_Event *event);
	virtual void handleMouseMotion(SDL_Event *event);
	virtual void handleMouseDown(SDL_Event *event);
	virtual void handleMouseUp(SDL_Event *event);
	virtual void update(float dt);
	virtual void render();
protected:
	float timestamp();
	void toggleFullscreen();
	void handleResize(SDL_Event *event);
	void handleEvents();
	bool initialized;
	bool running;
	SDL_Window *screen;
	SDL_Renderer *renderer;
};

}
