#include <fstream>
#include <iostream>
#include <sstream>

#include "Mesh.h"

namespace T3ddy
{

Mesh::Mesh()
{
	angle = 0.0f;
	scale = Vec3(1.0f, 1.0f, 1.0f);
	rotation = Vec3(1.0f, 0.0f, 1.0f);
	position = Vec3(0.0f, 0.0f, -1.0f);
	velocity = Vec3(0.0f, 0.0f, 0.0f);
}

Mesh::Mesh(const Mesh *mesh)
{
	vertices = mesh->vertices;
	texCoords = mesh->texCoords;
	normals = mesh->normals;
	colors = mesh->colors;
	indices = mesh->indices;
	modelMatrix = mesh->modelMatrix;
	angle = mesh->angle;
	scale = mesh->scale;
	rotation = mesh->rotation;
	position = mesh->position;
	velocity = mesh->velocity;
}

Mesh::~Mesh()
{
}

void Mesh::update(GLfloat dt)
{
	position += velocity * dt;

	modelMatrix = Mat4::makeTranslate(position) * Mat4::makeRotate(angle, rotation) * Mat4::makeScale(scale);

	needUpdate = false;
}

void Mesh::render(const RenderParams& params)
{
	if (needUpdate)
		update(0);

	Mat4 MV = params.viewMatrix * modelMatrix;
	Mat3 MVinv = transpose(invert(MV.getRotationMatrix()));

	params.shader->bind();

	params.shader->setUniform("uProjectionMatrix", params.projectionMatrix);
	params.shader->setUniform("uModelViewMatrix", MV);
	params.shader->setUniform("uNormalMatrix", MVinv);

	params.shader->setVertexAttribPointer("aVertexPosition", 3, GL_FLOAT, GL_FALSE, 0, &vertices[0]);
	params.shader->setVertexAttribPointer("aVertexColor", 3, GL_FLOAT, GL_FALSE, 0, &colors[0]);
	params.shader->setVertexAttribPointer("aVertexNormal", 3, GL_FLOAT, GL_FALSE, 0, &normals[0]);
	params.shader->setVertexAttribPointer("aTextureCoordinate", 2, GL_FLOAT, GL_FALSE, 0, &texCoords[0]);

	params.shader->setUniform("uLightPosition", params.lightPosition);

	params.shader->setUniform("uAmbientColor", params.ambientColor);
	params.shader->setUniform("uDiffuseColor", params.diffuseColor);
	params.shader->setUniform("uSpecularColor", params.specularColor);
	params.shader->setUniform("uShininess", params.shininess);

	params.shader->setUniform("uSkyColor", params.skyColor);
	params.shader->setUniform("uGroundColor", params.groundColor);

	params.shader->setUniform("uColor", params.solidColor);

	params.shader->setUniform("uTime", params.time);

	params.shader->setUniform("uScreenWidth", params.resolution.x);
	params.shader->setUniform("uScreenHeight", params.resolution.y);

	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, &indices[0]);
}

std::vector<std::string> Mesh::splitString(const std::string& s, const std::string& delim)
{
	std::vector<std::string> v;
	std::string s0 = s;
	std::string::size_type end(0);
	do
	{
		end = s0.find(delim);
		if (end != std::string::npos)
		{
			v.push_back(s0.substr(0, end));
			s0 = s0.substr(end + delim.length());
		}
		else
		{
			v.push_back(s0);
		}
	} while (end != std::string::npos);
	return v;
}

Mesh *Mesh::fromFile(const char *path)
{
	std::fstream in(path);

	if (!in.is_open())
		return 0;

	Mesh *mesh = new Mesh();

	if (!mesh)
		return 0;

	std::string line;

	while (std::getline(in, line))
	{
		std::vector<std::string> tokens = Mesh::splitString(line, " ");

		if (tokens[0] == "v")
		{
			GLfloat x = std::stof(tokens[1]);
			GLfloat y = std::stof(tokens[2]);
			GLfloat z = std::stof(tokens[3]);
			mesh->vertices.push_back(x);
			mesh->vertices.push_back(y);
			mesh->vertices.push_back(z);
		}
		else if (tokens[0] == "vn")
		{
			GLfloat nx = std::stof(tokens[1]);
			GLfloat ny = std::stof(tokens[2]);
			GLfloat nz = std::stof(tokens[3]);
			mesh->normals.push_back(nx);
			mesh->normals.push_back(ny);
			mesh->normals.push_back(nz);
		}
		else if (tokens[0] == "vt")
		{
			GLfloat u = std::stof(tokens[1]);
			GLfloat v = std::stof(tokens[2]);
			mesh->texCoords.push_back(u);
			mesh->texCoords.push_back(v);
		}
		else if (tokens[0] == "f")
		{
			std::vector<std::string> v0 = Mesh::splitString(tokens[1], "/");
			std::vector<std::string> v1 = Mesh::splitString(tokens[2], "/");
			std::vector<std::string> v2 = Mesh::splitString(tokens[3], "/");
			mesh->indices.push_back(std::stoi(v0[0])-1);
			mesh->indices.push_back(std::stoi(v1[0])-1);
			mesh->indices.push_back(std::stoi(v2[0])-1);
		}
	}

	in.close();

	return mesh;
}

}
