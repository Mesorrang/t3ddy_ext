uniform vec3 uLightPosition;
uniform vec3 uAmbientColor;
uniform vec3 uDiffuseColor;
uniform vec3 uSpecularColor;
uniform float uShininess;

varying vec3 vNormal;
varying vec3 vPosition;

#define INTENSITY_TO_SHADE
#define SPECULAR_TO_SHADE

float IntensityToShade(float Intensity)
{
#ifdef INTENSITY_TO_SHADE
	if (Intensity > 0.75)
		return 1.0;
	else if (Intensity > 0.5)
		return 0.667;
	else if (Intensity > 0.25)
		return 0.333;
	else
		return 0.0;
#else
	return Intensity;
#endif
}

float SpecularToShade(float Specular)
{
#ifdef SPECULAR_TO_SHADE
	if (Specular > 0.75)
		return 1.0;
	else if (Specular > 0.5)
		return 0.667;
	else if (Specular > 0.25)
		return 0.333;
	else
		return 0.0;
#else
	return Specular;
#endif
}

void main()
{
	vec3 Normal = normalize(vNormal);
	vec3 LightDirection = normalize(uLightPosition - vPosition);
	vec3 ReflectDirection = reflect(-LightDirection, Normal);
	vec3 ViewDirection = normalize(-vPosition);

	float Intensity = max(dot(Normal, LightDirection), 0.0);
	float Specular = 0.0;

	if (Intensity > 0.0)
	{
		float SpecularAngle = max(dot(ReflectDirection, ViewDirection), 0.0);
		Specular = pow(SpecularAngle, uShininess);
	}

	gl_FragColor = vec4(uAmbientColor + IntensityToShade(Intensity) * uDiffuseColor + SpecularToShade(Specular) * uSpecularColor, 1.0);
}