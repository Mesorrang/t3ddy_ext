#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "T3ddy.h"

using namespace T3ddy;

class Ovela: public Game
{
public:
	Ovela()
	{
	}

	~Ovela()
	{
		delete lightSphere;
		delete weird;
		delete dummy;
		delete cel;
	}

	bool init(int width, int height, bool fullscreen, bool enableSwapControl)
	{
		if (!Game::init("Cel shader with moving light source", width, height, fullscreen, enableSwapControl))
			return false;

		cel = Shader::fromFile("Data/cel.vs", "Data/cel.fs");

		if (!cel || !cel->isValid())
			return false;

		dummy = Shader::fromFile("Data/default.vs", "Data/default.fs");

		if (!dummy || !dummy->isValid())
			return false;

		weird = Mesh::fromFile("Data/weird.obj");

		if (!weird)
			return false;

		lightSphere = Mesh::fromFile("Data/sphere.obj");

		if (!lightSphere)
			return false;

		lightSphere->setScale(0.1f);

		srand(SDL_GetTicks());
		diffuseColor = Vec3::fromRGB(rand()&0xFFFFFF);
		deltaDiffuse = Vec3(-0.001,+0.001,-0.001);

		camera.setPosition(Vec3(0.0f, 0.0f, 0.0f));

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0f);

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK);

		return true;
	}

	void update(float dt)
	{
		camera.updateOrientation(dt*deltaHorizontalAngle, dt*deltaVerticalAngle);
		camera.updatePosition(dt*cameraMovement);

		GLfloat t = Game::timestamp();
		GLfloat a = 1.0;
		GLfloat b = 2.0;
		GLfloat c = 5.0;

		weird->setAngle(10*t);
		weird->setRotation(Vec3(1,1,0));

		lightPosition = Vec3(c*sinf(a*t/2.0)*cosf(b*t/2.0),0.0f,c*sinf(a*t/2.0)*sinf(b*t/2.0));
		lightSphere->setPosition(lightPosition);

		GLfloat dx = diffuseColor.x;
		GLfloat dy = diffuseColor.y;
		GLfloat dz = diffuseColor.z;

		if (dx+deltaDiffuse.x < 0||dx+deltaDiffuse.x>1) deltaDiffuse.x=-deltaDiffuse.x;
		if (dy+deltaDiffuse.y < 0||dy+deltaDiffuse.y>1) deltaDiffuse.y=-deltaDiffuse.y;
		if (dz+deltaDiffuse.z < 0||dz+deltaDiffuse.z>1) deltaDiffuse.z=-deltaDiffuse.z;

		diffuseColor += deltaDiffuse;
	}

	void render()
	{
		Vec3 ambientColor = Vec3::fromRGB(0x000033);

		glClearColor(ambientColor.x, ambientColor.y, ambientColor.z, 1.0f);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		int width, height;
		SDL_GetWindowSize(screen, &width, &height);

		GLfloat aspect = (GLfloat)width / (GLfloat)height;

		Mesh::RenderParams params;

		params.shader = this->cel;
		params.projectionMatrix = Mat4::makePerspective(45.0f, aspect, 1.0f, 100.0f);
		params.viewMatrix = camera.getViewMatrix();

		params.lightPosition = camera.getViewMatrix()*lightPosition;
		params.ambientColor = ambientColor;
		params.diffuseColor = diffuseColor;
		params.specularColor = Vec3::fromRGB(0xFFFFFF);
		params.shininess = 15.0f;

		weird->render(params);

		params.shader = this->dummy;
		params.solidColor = Vec3::fromRGB(0xFFFFFF);

		lightSphere->render(params);

		SDL_GL_SwapWindow(screen);
	}

	void handleKeyDown(SDL_Event *event)
	{
		SDL_Keycode key = event->key.keysym.sym;

		switch (key)
		{
		case SDLK_w:
			cameraMovement.z = +1.0f;
			break;
		case SDLK_a:
			cameraMovement.x = -1.0f;
			break;
		case SDLK_s:
			cameraMovement.z = -1.0f;
			break;
		case SDLK_d:
			cameraMovement.x = +1.0f;
			break;
		case SDLK_PAGEUP:
			cameraMovement.y = +1.0f;
			break;
		case SDLK_PAGEDOWN:
			cameraMovement.y = -1.0f;
			break;
		default:
			Game::handleKeyDown(event);
			break;
		}
	}

	void handleKeyUp(SDL_Event *event)
	{
		SDL_Keycode key = event->key.keysym.sym;

		switch (key)
		{
		case SDLK_w:
			cameraMovement.z = 0.0f;
			break;
		case SDLK_a:
			cameraMovement.x = 0.0f;
			break;
		case SDLK_s:
			cameraMovement.z = 0.0f;
			break;
		case SDLK_d:
			cameraMovement.x = 0.0f;
			break;
		case SDLK_PAGEUP:
			cameraMovement.y = 0.0f;
			break;
		case SDLK_PAGEDOWN:
			cameraMovement.y = 0.0f;
			break;
		case SDLK_SPACE:
			int width, height;
			SDL_GetWindowSize(screen, &width, &height);
			SDL_WarpMouseInWindow(screen, width/2, height/2);
			break;
		default:
			Game::handleKeyUp(event);
			break;
		}
	}

	void handleMouseMotion(SDL_Event *event)
	{
		GLfloat x = event->motion.x;
		GLfloat y = event->motion.y;

		int width, height;
		SDL_GetWindowSize(screen, &width, &height);

		GLfloat w = (GLfloat)width;
		GLfloat h = (GLfloat)height;

		GLfloat sensitivity = 50.0f;

		deltaHorizontalAngle = sensitivity * Tools::smoothstep(x, 0.0f, -1.0f, w, +1.0f);
		deltaVerticalAngle = sensitivity * Tools::smoothstep(y, 0.0f, -1.0f, h, +1.0f);
	}

	void handleMouseDown(SDL_Event *event)
	{
		switch (event->button.button)
		{
		case SDL_BUTTON_LEFT:
			cameraMovement.z = +1.0f;
			break;
		case SDL_BUTTON_MIDDLE:
			camera.setPosition(Vec3(0.0f, 0.0f, 3.0f));
			camera.setDirection(Vec3(0.0f, 0.0f, -1.0f));

			int width, height;
			SDL_GetWindowSize(screen, &width, &height);
			SDL_WarpMouseInWindow(screen, width/2, height/2);
			break;
		case SDL_BUTTON_RIGHT:
			cameraMovement.z = -1.0f;
			break;
		default:
			break;
		}
	}

	void handleMouseUp(SDL_Event *event)
	{
		cameraMovement.z = 0.0f;
	}

private:
	Mesh *lightSphere;
	Mesh *weird;
	Shader *dummy;
	Shader *cel;
	Vec3 diffuseColor;
	Vec3 deltaDiffuse;
	Camera camera;
	GLfloat deltaHorizontalAngle;
	GLfloat deltaVerticalAngle;
	Vec3 cameraMovement;
	Vec3 lightPosition;
};

void usage(void)
{
	printf("Options:\n");
	printf("-w WxH: set window size\n");
	printf("-f: enable fullscreen\n");
	printf("-v: enable GL swap control\n");
	exit(0);
}

int main(int argc, char **argv)
{
	int width = 1280;
	int height = 720;
	bool fullscreen = false;
	bool enableSwapControl = false;
	int c;

	while ((c = getopt(argc, argv, "w:fv")) != EOF)
	{
		switch (c)
		{
		case 'w':
			sscanf(optarg, "%dx%d", &width, &height);
			break;
		case 'f':
			fullscreen = true;
			break;
		case 'v':
			enableSwapControl = true;
			break;
		default:
			usage();
			break;
		}
	}

	Ovela *o = new Ovela();
	if (o->init(width, height, fullscreen, enableSwapControl))
		o->run();
	delete o;
	return 0;
}
